KDE4
====

This repository holds all of the build scripts and build sequence for the
KDE 4.x  desktop environment on Vector | VLocity linux.  The script in
the ``src/`` directory should trigger the complete build in the correct
sequence.


WARNING
=======

**DO NOT RUN THIS SCRIPT IN A PRODUCTION SYSTEM**.  This will install every
package built on the existing system.  Use a sandbox environment (vlbuildbot)
to test these scripts before submitting a build request to the vlbuildbot.

